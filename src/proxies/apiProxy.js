//
// File: apiProxy.js
// Auth: Martin Burolla
// Date: 1/16/2022
// Desc: The one and only access point to our API: jsonplaceholder.typicode.com.
//

//
// GET: http://jsonplaceholder.typicode.com/users
//

import axios from "axios";

export const getUserName = async () => {
    try {
        let res = await axios.get('http://jsonplaceholder.typicode.com/users/');
        return res.data[0].name;
    }
    catch(err) {
        console.log(err);
    }
};

//
// GET: http://jsonplaceholder.typicode.com/users/{id}
//

export const getUserNameById = async (personId) => {
    try {
        let res = await axios.get(`http://jsonplaceholder.typicode.com/users/${personId}`);
        return res.data.name;
    }
    catch(err) {
        console.log(err);
    }
};

//
// GET: This is a mock.
//

export const getUrlForPersonId = async (personId) => {

    const urlArray = [
        "https://tinyurl.com/2p8j2um8",
        "https://ik.imagekit.io/rpx89d1qjay/images/4cefe875-2b8c-4fb6-bbe2-3eb07f8ecc2b.d087264a3799ec5583ce841042cb6d8a_CamL8S-kN.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528924129",
        "https://ik.imagekit.io/rpx89d1qjay/images/5c9601b4-adf8-4231-9d08-b46781029477.242e0314bc2f71d1efe2f0dc2996a6bb_ONVngCAi3V.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528924248",
        "https://ik.imagekit.io/rpx89d1qjay/images/48bc093d-73f9-414c-a174-58e4f02b003b.dd588813fcdb77cfe931e24bfcbd2602_Z8r_PbjAa.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528924057",
        "https://ik.imagekit.io/rpx89d1qjay/images/26dd747c-94e0-4cc6-902b-6a2925afed15.cab50608687097604ac8c8d203102b77_5zbRsCPyJu.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528923820",
        "https://ik.imagekit.io/rpx89d1qjay/images/64381468-fbaa-4410-950f-017ff65f368c.8994a8c310488eea85d6d3f40b5a6fa4_Omtov8nxPl.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528919701",
        "https://ik.imagekit.io/rpx89d1qjay/images/9469b220-48ed-4590-ad89-4d59f0d98acf.453f4fdbc6d519818811d7fe25c18b6f_ljsKQHzUZ0V6.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528919885",
        "https://ik.imagekit.io/rpx89d1qjay/images/36444f6a-6fa7-48ed-ba29-9cea68bce972.4a0277b7e29506bda414c33c9e59381e_QcDtTAjg0CW.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528919535",
        "https://ik.imagekit.io/rpx89d1qjay/images/7f6c40b6-352d-46e2-b7be-bc24a10add7e.d1f2c88a0374075ecca193f235c7c586_ZVBpxRMt7s3.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528919808",
        "https://ik.imagekit.io/rpx89d1qjay/images/32202a21-3bc5-46b3-b587-4f84fd1b563b.ba4492bb6eec73e9dbea8411be0798f6_NPbNsF2U5.jpeg?ik-sdk-version=javascript-1.4.3&updatedAt=1642528919526",
        "https://tinyurl.com/2p8j2um8"
    ]

    return urlArray[personId];
}

//
// PUT
//

//
// POST
//

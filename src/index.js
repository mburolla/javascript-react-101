import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
//import './css/bootstrap.min.css'
import App from './components/App';

// ========================================

ReactDOM.render(<App />, document.getElementById('root'));
// No service worker here, this is a SPA not a PWA

import React from 'react';

export default class PersonInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="personInfo">PersonInfo
                <table>
                    <tbody>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                        </tr>
                        <tr>
                            <td>{this.props.counter}</td>
                            <td>{this.props.username}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        ); 
    }
}

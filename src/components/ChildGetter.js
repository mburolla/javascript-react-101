import React from 'react';
import { getUrlForPersonId } from '../proxies/apiProxy';

export default class ChildGetter extends React.Component {

    //
    // Constructors
    //

    constructor(props) {
        super(props);
        this.state = {
            url: null,
            disableButton: true
        };
    }

    //
    // Render
    //

    render() {
        return (
            <div className="childgetter">ChildGetter: {this.props.username}
                <br/>
                <img src={this.state.url} alt="" width="100"></img>
                <br/>
                <button disabled={this.state.disableButton} onClick={this.handleClickBuy}>Buy</button>&nbsp;
            </div>
        ); 
    }
    
    //
    // Lifecycle
    //

    async componentDidUpdate() { 
        let url = await getUrlForPersonId(this.props.personId);
        if (url !== this.state.url && this.props.personId > 0) { // Prevents an infinite loop.
            this.setState({url: url});
            this.setState({disableButton: false})
        }
    }

    //
    // Handlers
    //

    handleClickBuy = () => {
        this.props.onBuy(this.props.personId);
        this.setState({disableButton: true})
    }
}

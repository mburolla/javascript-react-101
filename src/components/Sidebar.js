import React from 'react';

export default class Sidebar extends React.Component {

    //
    // Constructors
    //

    constructor(props) {
        super(props);
        this.state = {
            numberItems: 0 // Not used
        };
    }

    // This is handy if you want to set a state variable when a property is passed into a component.
    // This is kept here for illustration purposes only.
    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            numberItems: nextProps.personIds.length // Not used
        };
    }

    //
    // Render
    //

    render() {

        // Simulate an error that will be caught by the ErrorBoundry component.
        // if (this.props.personIds.length === 3) {
        //     throw new Error('Uh oh... bad things happen in threes');
        // }

        if (!this.props.show) {
            return null;
        }

        if (this.props.show) {
            return (
                <div className="sidebar">
                    You have {this.props.personIds.length} items
                    <table>
                        <tbody>
                        {
                            this.props.personIds.map(i => 
                                <tr key={i}>
                                    <td>
                                        personid
                                    </td>
                                    <td>
                                        {i}
                                    </td>
                                    <td>
                                        <button onClick={() => this.handleRemoveItem(i)}>-</button>
                                    </td>
                                </tr>)
                        }
                        </tbody>
                    </table>
                    <br/>
                    <button onClick={this.handleCheckout}>Checkout</button>&nbsp;
                </div>
            ); 
        } 
    }

    //
    // Handlers
    //

    handleCheckout = () => {
        console.log('Purchase these items: ' + this.props.personIds);
    }

    handleRemoveItem = (id) => {
        this.props.onRemoveItem(id); // Received by the parent: Getter.
    }
}

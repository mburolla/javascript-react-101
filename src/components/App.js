import React from 'react';
import Getter from './Getter';
import Identity from './Identity';
//import {BrowserRouter as Router, Route} from "react-router-dom";

export default class App extends React.Component {
    render() {
        // <Router>
        //     <Header />
        //     <Route exact path = "/" component={EventList} />
        //     <Route exact path = "/cart" component={Shoppingcart} />
        //     <Route exact path = "/confirm" components={Confirmation} />
        // </Router>
        return (
            <div className="game">
                <Getter />
                <Identity />
            </div>
        );
    }
}

import React from 'react';

export default class Square extends React.Component {

    //
    // Constructors
    //

    constructor(props) {
        super(props);
        this.state = {
          value: null,
        };
    }

    //
    // Render
    //

    render() {
        return (
            <button
              className="square"
              onClick={() => this.setState({value: 'X'})}
            >
              {this.props.value}
            </button>
          ); 
    }
}

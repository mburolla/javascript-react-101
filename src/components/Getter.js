import React from 'react';
import Sidebar from './Sidebar';
import PersonInfo from './PersonInfo';
import ChildGetter from './ChildGetter';
import ErrorBoundary from './ErrorBoundry';
import { getUserNameById } from '../proxies/apiProxy';

export default class Getter extends React.Component {

    //
    // Constructors
    //

    constructor(props) {
        super(props);
        this.state = {
          counter: 0,
          username: '',
          showSidebar: false,
          personIds: [] // These are the one and only IDs for the shopping cart.
        };
    }

    //
    // render
    //

    render() {
        return (
            <div className="getter">
                <table >
                    <tbody>
                        <tr>
                            <td>
                                <img src={require('../assets/check.png')} alt="" width="11"/>&nbsp;
                                <span>I'm a Getter.  I get things.</span>
                                <br />
                                <button onClick={this.handleClickPerson}>Get Person</button>&nbsp;
                                <button onClick={this.handleClickSidebar}>Shopping Cart</button>
                                <br />
                                <PersonInfo counter={this.state.counter} username={this.state.username}/>
                                <ChildGetter onBuy={this.handleBuy} username={this.state.username} personId={this.state.counter}/>
                            </td>
                            <td>
                                <ErrorBoundary>
                                    <Sidebar onRemoveItem={this.handleRemoveItem} show={this.state.showSidebar} personIds={this.state.personIds}/>
                                </ErrorBoundary>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }    

    //
    // Handlers
    //

    handleClickPerson = async () => {
        const personId = this.state.counter + 1;
        this.setState({counter: personId});
        const username = await getUserNameById(personId);
        this.setState({username: username});
    }

    handleClickSidebar = () => {
        this.setState({showSidebar: !this.state.showSidebar});
    }

    //
    // Child callbacks
    //

    handleBuy = (personId) => { // Fired by ChildGetter.
        this.setState({selectedPersonId: this.state.personIds.push(personId)}); // Hack: cannot do: this.state.personIds.push(personId);
    }

    handleRemoveItem = (id) => { // Fired by Sidebar.
        const newList = this.state.personIds.filter(item => item !== id);
        this.setState({personIds: newList});
    }
}

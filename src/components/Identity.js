import React from 'react';

export default class Indentity extends React.Component {

    //
    // Constructors
    //

    constructor(props) {
        super(props);
        this.state = {
            firstname: ''
        };
    }

    //
    // Render
    //

    render() {
        return (
            <div className="identity">
                <form>
                    <input type="text" name="firstname" placeholder="First Name" onChange={this.handleFieldChange} value={this.state.firstname}></input>
                </form>
                <button disabled={this.state.disableButton} onClick={this.handleSubmit}>Submit</button>&nbsp;
            </div>
          ); 
    }

    //
    // Handlers
    //

    handleFieldChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }    

    handleSubmit = () => {
        console.log('Send to server: ' + this.state.firstname)
    }
}

# JavaScript React 101
Helper project that illustrates the architecture of a React web application that uses a ReST API and a parent-child relationship between React components to build a shopping cart.

# Getting Started
- Clone this repo
- Get the project dependencies: `npm install`
- Execute: `npm start`

# Overview
The UI is intentionally ugly to show the individual React components.

![](./docs/app.PNG)

This diagram below is color coded to match the UI above.  The Getter is the parent and the children are:
- ChildGetter
- PersonInfo
- SideBar

![](./docs/diagram.PNG)

# Lifecycle
https://www.w3schools.com/react/react_lifecycle.asp
### Mounting
- `constructor()`
- `getDerivedStateFromProps()`
- `render()`
- `componentDidMount()`
- [UseEffect](https://reactjs.org/docs/hooks-effect.html)

### Updating
- `getDerivedStateFromProps()`
- `shouldComponentUpdate()`
- `render()`
- `getSnapshotBeforeUpdate()`
- `componentDidUpdate()`
- [UseEffect](https://reactjs.org/docs/hooks-effect.html)

### Unmounting
- `componentWillUnmount()`

# PluralSight Notes
[React Fundamentals](
https://app.pluralsight.com/course-player?clipId=890ab27a-6721-4b24-bcae-07c523c4a9bd)

Angular vs React
----------------
- All in one solution vs create your own
- TypeScript vs JavaScript
- 2-way data binding vs 1-way data binding
- Powerful HTML templates vs Using JavaScript to create HTML

React Overview
------------------
- What is React?  
  - Single Page Application (SPA) vs Progressive Web App (PWA)
  - React is an abstraction library (not a framework) that targets the web browser DOM to support the rendering of UI elements and events
  - React Virtual DOM is an abstraction layer over the real browser DOM
  - If you want more libraries in your app, simply leverage NPM and run `npm install <your favorite lib>`
  - React apps are highly customizable
- One-way data binding: 
  - Changes to the model (state) are reflected in the UI: Model(state) -> UI
  - React _reacts_ to changes in the model (state) and displays the model in the UI
  - Calls to `this.setState()` will eventually render in the UI
  - `componentDidUpdate()` is fired when the state has changed
  - Calling `this.setState()` in `componentDidUpdate()` without a condition causes an infinite loop!
- The UI is only changed by changing the state of a Component
- As much as possible, you should design Components that don't use state
 
Components
----------
- Components come in [two flavors](https://www.telerik.com/blogs/react-class-component-vs-functional-component-how-choose-whats-difference?kw=&cpn=15387578594&&utm_source=google&utm_medium=cpc&utm_campaign=kendo-ui-react-trial-search-bms-NA&ad_group=DSA+Ad+Group&utm_term=DYNAMIC+SEARCH+ADS&ad_copy=&ad_type=DSA&ad_size=&ad_placement=&gclid=CjwKCAiA866PBhAYEiwANkIneCBWh5f3YivZTiaPEWRCX4MKd7A_BoHUtf8eojVslE5tlJH_hVHo-xoCkCQQAvD_BwE&gclsrc=aw.ds): Function or Class based

#### Function Components (Modern approach)

```
import { useState } from 'react';
const [personId, setPersonId] = useState(true);

function Welcome(props) {
    return (
        <h1>Hello, {props.name}</h1>
    );
}
``` 

#### Class Components (Classic approach)

```
class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.personId = 0;
    }

    render() {
        return (
            <h1>Hello, {props.name}</h1>
        );
    }
}
``` 

- Components are buidling blocks that when combined together create a UI
- React Components MUST begin with a capital letter
- Hooks cannot be used in Class Components, but can be used in Function Components
- Components should favor the [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) (SRP)

- Props 
  - As of React 16, props are READ ONLY from within a Component
  - Props stay the same for the component lifetime
  - Props are passed into a Component by it's parent
  - If the props have not changed, then a Component is not re-rendered
  - Type Checking Validation: propTypes runtime validation of properties
    - `npm install prop-types`
    - `import PropTypes from 'prop-types'`
  - `getDerivedStateFromProps()` can be used to set state when a property is set
  - Values and callback functions are passed down from the parent to child 
  Component properties
  - "Prop drilling" referes to passing properties down from one Component to another, not best practice and can be avoided using the React context API

- State 
  - State can only be passed down (as props) from a parent component to a child component
  - State can only be declared inside a React Component
  - State is local mutable data within the Component that can "survive" re-renders
  - `this.setState()` causes the Component to be re-rendered eventually
  - Setting the state via properties is done by: `getDerivedStateFromProps()`
  - `useState([])` is used in Functional Components whereas `this.setState()` is used in Class Components
  - Keep the state as close as possible to where it is actually being used
  - "Lifting state" refers to moving state variables up one or more parent levels
  - [YouTube](https://youtu.be/ipHlH4VE7Wo)

- Testing
  - Components are tested via Jest executing `npm test`
  - Jest runs continuously in the background, much like `npm start`

Events
------
- Disable the default behavior of things like checkboxes:
  - `onClick={ (e) => {e.preventDefault();} }`

Forms
-----
- Form elements must be bound to a Component's state or else you won't be able to type text in the form field
- There are many form libraries to aid in validation and submission
- MUCH easier than Spring MVC forms
- [ReadThis](https://rangle.io/blog/simplifying-controlled-inputs-with-hooks/)

Client-side Routing with HTML5 pushState
----------------------------------------
- React Router
  - Conditional Rendering based on routes
  - `<Route exact path="/about" component={About}>` Routes to the `About` React Component
  - `<Route exact path="/" component={Home}>` Routes to the `Home` React Component
  - `<Route path="/single/:id" component={Single}>`

State
-----
- A UI should be derived entirely from an applications state
- Model-view-intent Architecture (Model-->View-->Intent-->Model... etc)

# Modern Web Applications
-------------------------
- [Session Tokens](https://blog.ropnop.com/storing-tokens-in-browser/)

# Other
-------
[UseEffect](https://reactjs.org/docs/hooks-effect.html) Only works for Class Components
https://youtu.be/0ZJgIjIuY7U
[UseContext](https://www.telerik.com/blogs/understand-react-context-api?kw=&cpn=15387578594&&utm_source=google&utm_medium=cpc&utm_campaign=kendo-ui-react-trial-search-bms-NA&ad_group=DSA+Ad+Group&utm_term=DYNAMIC+SEARCH+ADS&ad_copy=&ad_type=DSA&ad_size=&ad_placement=&gclid=CjwKCAiA0KmPBhBqEiwAJqKK43u7Oke3CP1uDvW_Pq4p3E800XPd7pM-I_xipNGA83ZydJmgQXymhxoCDE4QAvD_BwE&gclsrc=aw.ds) Anti-pattern?  Feels like a global singleton.

# PluralSight: Calling APIs with React
--------------------------------------
[Course](https://app.pluralsight.com/course-player?clipId=0b307c19-62b1-4cb8-805c-bee597701664)
-  Where to put API calls?
  - `componentDidMount()`
  - `useEffect()`
- Libraries
  - Fetch API
  - axios
  - jQuery/XMLHttpRequest
  - [SWR](https://swr.vercel.app/)
    - SWR is a strategy to first return the data from cache (stale), then send the fetch request (revalidate), and finally come with the up-to-date data.
- Mock APIs with [JSON-Server](https://www.npmjs.com/package/json-server)
- [ErrorBoundries](https://reactjs.org/docs/error-boundaries.html)
- Suspense is coming... https://reactjs.org/docs/concurrent-mode-suspense.html

# PluralSight: Designing React Components
[Course](https://app.pluralsight.com/library/courses/react-components-designing/table-of-contents)
- Use spread (...) to pass data into Components and destructure ({}) to select the data elements we are interested in consuming
- `useEffect()` is called after a Component renders  
- `const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));`
- `npm install react-placeholder`... <ReactPlaceHolder></ReactPlaceHolder>
- Hooks can contain other hooks

# PluralSight: Styling React Components
[Course](https://app.pluralsight.com/course-player?clipId=c30554e7-8894-4f71-a224-992316edf9fa)
- Inline Styling
- CSS Stylesheets
- CSS-in-JS libraries (styled-components, emotion, aphrodite, glamor, styled-jsx, radium, astroturf, many more...)
  - Styled Components:
    - Places the styling code with the React Component code
    - `npm install styled-components`
    - `import styled, {keyframes} from 'styled-components'`
      - jitter
- CSS Modules
  - Organizes CSS code into separate module files
  - `import css from './Newsletter.module.css'`
  - `<header className={css.header}>`

# Where to Put API Code?
- Class Components: `componentDidMount()`
- Function Components: Hook: `useEffect()`

# Analyze This...
Taken from [here](https://reactjs.org/docs/conditional-rendering.html)
```
function WarningBanner(props) {
  if (!props.warn) {
    return null;
  }

  return (
    <div className="warning">
      Warning!
    </div>
  );
}

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showWarning: true};
    this.handleToggleClick = this.handleToggleClick.bind(this);
  }

  handleToggleClick() {
    this.setState(state => ({
      showWarning: !state.showWarning
    }));
  }

  render() {
    return (
      <div>
        <WarningBanner warn={this.state.showWarning} />
        <button onClick={this.handleToggleClick}>
          {this.state.showWarning ? 'Hide' : 'Show'}
        </button>
      </div>
    );
  }
}

ReactDOM.render(
  <Page />,
  document.getElementById('root')
);
```

# Build Notes
- "react": "^17.0.2",
- "react-dom": "^17.0.2",
- "react-scripts": "5.0.0"
